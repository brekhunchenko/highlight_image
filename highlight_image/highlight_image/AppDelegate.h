//
//  AppDelegate.h
//  highlight_image
//
//  Created by Yaroslav Brekhunchenko on 7/3/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

