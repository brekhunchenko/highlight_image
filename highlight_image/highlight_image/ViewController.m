//
//  ViewController.m
//  highlight_image
//
//  Created by Yaroslav Brekhunchenko on 7/3/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "ViewController.h"
#import "HighlightMusicSheetImageView.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet HighlightMusicSheetImageView *highlightMusicSheetImageView;

@end

@implementation ViewController

#pragma mark - UIViewController Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self _setupHighlightMusicSheetImageView];
}

#pragma mark - Setup

- (void)_setupHighlightMusicSheetImageView {
    _highlightMusicSheetImageView.musicSheetImage = [UIImage imageNamed:@"Image-PDF"];
}

#pragma mark - Actions

- (IBAction)highlightRectButton:(id)sender {
    [_highlightMusicSheetImageView highlightRect:CGRectMake(126.0f/2 + 75.0f, 288.0f/2, 140.0f, 50.0f) animationDuration:1.5];
}

- (IBAction)resetButtonAction:(id)sender {
    [_highlightMusicSheetImageView resetHighlights];
}

@end
