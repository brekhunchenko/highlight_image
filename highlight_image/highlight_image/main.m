//
//  main.m
//  highlight_image
//
//  Created by Yaroslav Brekhunchenko on 7/3/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
