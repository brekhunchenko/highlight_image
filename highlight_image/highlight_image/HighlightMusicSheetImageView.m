//
//  HighlightMusicSheetImageView.m
//  highlight_image
//
//  Created by Yaroslav Brekhunchenko on 7/3/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import "HighlightMusicSheetImageView.h"

@interface HighlightMusicSheetImageView()

@property (nonatomic, strong) UIImageView* musicSheetImageView;

@property (nonatomic, strong) NSMutableArray<UIView *>* highlightRectanglesViews;

@end

@implementation HighlightMusicSheetImageView

#pragma mark - Initializers

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _highlightRectanglesViews = [NSMutableArray new];
        
        self.highlightColor = [UIColor greenColor];
        
        [self _setupMusicSheetImageView];
    }
    return self;
}

#pragma mark - Setup

- (void)_setupMusicSheetImageView {
    _musicSheetImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    _musicSheetImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self addSubview:_musicSheetImageView];
}

#pragma mark - Custom Setters & Getters

- (void)setMusicSheetImage:(UIImage *)musicSheetImage {
    _musicSheetImage = musicSheetImage;
    
    _musicSheetImageView.image = _musicSheetImage;
}

#pragma mark - Public Methods

- (void)highlightRect:(CGRect)rect animationDuration:(NSTimeInterval)duration {
    CGRect rect0 = CGRectMake(rect.origin.x, rect.origin.y, 1.0f, rect.size.height);
    UIView* viewRect = [[UIView alloc] initWithFrame:rect0];
    viewRect.backgroundColor = _highlightColor;
    [_highlightRectanglesViews addObject:viewRect];
    [self insertSubview:viewRect belowSubview:_musicSheetImageView];
    [UIView animateWithDuration:duration delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
        viewRect.frame = rect;
    } completion:nil];
}

- (void)resetHighlights {
    [_highlightRectanglesViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _highlightRectanglesViews = [NSMutableArray new];
}

@end
