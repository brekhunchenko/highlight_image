//
//  HighlightMusicSheetImageView.h
//  highlight_image
//
//  Created by Yaroslav Brekhunchenko on 7/3/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HighlightMusicSheetImageView : UIView

@property (nonatomic, strong) UIImage* musicSheetImage;
@property (nonatomic, strong) UIColor* highlightColor;

- (void)highlightRect:(CGRect)rect animationDuration:(NSTimeInterval)duration;
- (void)resetHighlights;

@end
